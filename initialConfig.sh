#!/bin/bash

[[ -z $LDAP_PASSWORD ]] && LDAP_PASSWORD='password'
[[ -z $LDAP_DOMAIN ]] && LDAP_DOMAIN='example.com'
[[ -z $LDAP_ORGANIZATION ]] && LDAP_ORGANIZATION='Earth Federation'
[[ -z $LDAP_BACKEND ]] && LDAP_BACKEND='MDB'

cat << EOF | debconf-set-selections
slapd slapd/internal/generated_adminpw password ${LDAP_PASSWORD}
slapd slapd/internal/adminpw password ${LDAP_PASSWORD}
slapd slapd/password2 password ${LDAP_PASSWORD}
slapd slapd/password1 password ${LDAP_PASSWORD}
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/domain string ${LDAP_DOMAIN}
slapd shared/organization string ${LDAP_ORGANIZATION}
slapd slapd/backend string ${LDAP_BACKEND}
slapd slapd/purge_database boolean true
slapd slapd/move_old_database boolean true
slapd slapd/allow_ldap_v2 boolean false
slapd slapd/no_configuration boolean false
slapd slapd/dump_database select when needed
EOF

dpkg-reconfigure -f noninteractive slapd
