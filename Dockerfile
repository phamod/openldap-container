FROM ubuntu:latest

WORKDIR /usr/src/app
COPY . .
RUN apt-get update -y

RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -y vim curl dos2unix ldap-utils slapd
RUN dos2unix *.sh

CMD ["./startup.sh"]
