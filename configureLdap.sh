#!/bin/bash

[[ -z $LDAP_PASSWORD ]] && LDAP_PASSWORD='password'
service slapd start

apt-get install ldap-utils slapd -y

ldapmodify -Y external -H ldapi:/// -f access-admin.ldif
ldapmodify -a -D cn=admin,dc=example,dc=com -w ${LDAP_PASSWORD} -f /etc/ldap/schema/ppolicy.ldif

ldapadd -x -D "cn=admin,dc=example,dc=com" -w ${LDAP_PASSWORD} -H ldap:// -f ldap.ldif 
ldapmodify -a -D cn=admin,dc=example,dc=com -w ${LDAP_PASSWORD} -f passwordPolicy.ldif
ldapmodify -a -D cn=admin,dc=example,dc=com -w ${LDAP_PASSWORD} -f acl.ldif

