#!/bin/bash

# used to reconfigure the default admin account and password
ldapUserDir="ou=users,dc=example,dc=com"

# if the configured files are missing reconfigure slapd
if test ! -f /etc/ldap/.slapdconf -a ! -f /var/lib/ldap/.slapdconf
then 
	echo 'true'

	# set the initial configuration settings see readme for more info
	./initialConfig.sh
	touch /etc/ldap/.slapdconf /var/lib/ldap/.slapdconf

	# change the domain of the server from example.com to the given
	if [[ ! -z $LDAP_DOMAIN ]]
	then
		IFS='.' read -r -a domain <<< "$LDAP_DOMAIN"
		directory="dc=${domain[0]}"
		# create the ldap distinguished name for the domain for configuring
		for  element in "${domain[@]:1}"
		do
			directory="${directory},dc=${element}"
		done
		sed -i s/dc=example,dc=com/$directory/g *.ldif
		sed -i s/dc=example,dc=com/$directory/g configureLdap.sh
		ldapUserDir="ou=users,${directory}"
	fi

	# set the initial admin user to use instead of root
	if [[ ! -z $LDAP_USER ]]
	then 
		sed -i s/cn=hamod/cn=${LDAP_USER}/g ldap.ldif
		ldapUserDir="cn=${LDAP_USER},${ldapUserDir}"
	else
		ldapUserDir="cn=hamod,${ldapUserDir}"
	fi

	# configure the server
	./configureLdap.sh


	# if the password has been set change
	if [[ ! -z $LDAP_USER_PASSWORD ]]
	then
		ldappasswd -D $ldapUserDir -w password -a password -s $LDAP_USER_PASSWORD
	fi
	echo 'done reconfiguring'
else
	service slapd start
	echo 'slapd started'
fi

# keeps container running while service is running
while [[ `service slapd status` == *"is running"* ]]; do
	sleep 1m
done
